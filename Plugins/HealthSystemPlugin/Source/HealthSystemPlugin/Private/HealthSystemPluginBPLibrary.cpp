// Copyright Epic Games, Inc. All Rights Reserved.

#include "HealthSystemPluginBPLibrary.h"
#include "HealthSystemPlugin.h"


UHealthSystemPluginBPLibrary::UHealthSystemPluginBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}


void UHealthSystemPluginBPLibrary::HealthSystemPluginHealthMod(const float HealthModifier, const float Health, float &FinalHealth, bool &IsDead)
{
	bool Dead = false;
	//FinalHealth = Health - HealthModifier;
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Helth"));
	
	FinalHealth = Health + HealthModifier;
	if (FinalHealth <= 0.0) {
		Dead = true;
		FinalHealth = 0;
	}
	if (FinalHealth >= Health) {
		FinalHealth = Health;
	};
	IsDead = Dead;
	
	//return true;
	
}