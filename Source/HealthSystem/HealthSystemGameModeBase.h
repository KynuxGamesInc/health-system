// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HealthSystemGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HEALTHSYSTEM_API AHealthSystemGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
