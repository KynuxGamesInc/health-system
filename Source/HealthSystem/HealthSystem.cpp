// Copyright Epic Games, Inc. All Rights Reserved.

#include "HealthSystem.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HealthSystem, "HealthSystem" );
